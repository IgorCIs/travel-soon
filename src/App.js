import React from 'react'
import styles from './app.module.sass'

const App = () => {
  return (
    <div className={styles.root}>
      <div className={styles.logo}>
        <img src='./logo.png' alt='iWareTravel'/>
      </div>
      <div className={styles.content}>
        <div className={styles.wrapper}>
          <div className={styles.title}>
            <div className={styles.title_top}> 
              Safe Travels,
            </div>
            <div className={styles.title_bottom}> 
              Begin With Travel Insurance.
            </div>
          </div>
          <div className={styles.banner}>
            <div className={styles.globe}>
              <img src='./globe.png' alt='globe'/>
              <div className={styles.text}>
                <div className={styles.text_wrapper}>
                  <div className={styles.text_top}>
                  Launching End of 2019 
                  </div>
                  <div className={styles.text_bottom}>
                  Get Travel Insurance in Just 60sec!
                  </div>
                </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default App
